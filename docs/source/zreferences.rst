References
==========


.. [Abraham07] Abraham, D. J., Blum, A., and Sandholm, T. (2007). Clearing algorithms for barter exchange markets: En-
        abling nationwide kidney exchanges. In Proceedings of the 8th ACM Conference on Electronic Commerce,
        EC ’07, page 295–304, New York, NY, USA. Association for Computing Machinery.

.. [Roth07] Roth, A. E., Sonmez, T., and  ̈Unver, M. U. (2007). Efficient kidney exchange: Coincidence of wants in
        markets with compatibility-based preferences. American Economic Review, 97(3):828–851

.. [Dickerson16] Dickerson, J. P., Manlove, D. F., Plaut, B., Sandholm, T., and Trimble, J. (2016). Position-indexed formula-
        tions for kidney exchange. In Proceedings of the 2016 ACM Conference on Economics and Computation,
        pages 25–42.

.. [Delorme23] Maxence Delorme, M., García, S., Gondzio, J., Kalcsics, J., Manlove, D., Pettersson, W. (2023).
        New Algorithms for Hierarchical Optimization in Kidney Exchange Programs. Operations Research. Vol 72, No. 4
