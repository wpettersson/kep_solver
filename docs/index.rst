Welcome to kep_solver's documentation!
======================================

.. automodule:: kep_solver

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   source/usage
   source/formats.rst
   source/terms.rst
   source/faq.rst
   source/kep_solver

.. include:: ../README.md
   :parser: myst_parser.sphinx_

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
